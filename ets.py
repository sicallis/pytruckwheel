from evdev import ecodes

# Where 'ETS' appears as a term, this can refer to 'ATS' or 'ETS2'

# ETS DEFAULTS
RightIndicator = ecodes.KEY_RIGHTBRACE
LeftIndicator = ecodes.KEY_LEFTBRACE
LightHorn = ecodes.KEY_J
Horn = ecodes.KEY_H
AirHorn = ecodes.KEY_N
Lights = ecodes.KEY_L
Wipers = ecodes.KEY_P
Beacon = ecodes.KEY_O
DifferentialLock = ecodes.KEY_V
CruiseControl = ecodes.KEY_C
Activate = ecodes.KEY_ENTER
LookLeft = ecodes.KEY_KPSLASH
LookRight = ecodes.KEY_KPASTERISK
Trailer = ecodes.KEY_T
Handbrake = ecodes.KEY_SPACE
Engine = ecodes.KEY_E

RouteAdvisorNextPage = ecodes.KEY_KP0
RouteAdvisorModes = ecodes.KEY_F3

InteriorCamera = ecodes.KEY_1
ChaseCamera = ecodes.KEY_2
NextCamera = ecodes.KEY_9

DebugCamera = ecodes.KEY_0
HighBeams = ecodes.KEY_K

DashboardDisplayInfo = ecodes.KEY_I

# MODIFIED ETS KEYS
# Unfortunately, this requires re-doing assignments on the numpad, which are limited in number
# Can get more from dropping unneeded interior camera controls

RouteAdvisorPreviousPage = ecodes.KEY_KP1
RouteAdvisorNavigationZoomIn = ecodes.KEY_KP2
RouteAdvisorNavigationZoomOut = ecodes.KEY_KP3

CruiseControlIncrease = ecodes.KEY_Z
CruiseControlDecrease = ecodes.KEY_X

DashboardMapMode = ecodes.KEY_SLASH
