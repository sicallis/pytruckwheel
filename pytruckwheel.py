#!/usr/bin/env python3
import g920
import ets
import evdev
import time
import signal

# 'ETS' used here will refers to both 'ATS' and 'ETS2'. This is intended to be a unified config

# TODO: Reconfigure the virtual controller to expose buttons and bind them in ETS/ATS instead. Avoids non-standard numpad layouts.
# Alternatively, only define virtual buttons for controls which are not bound by default

## Approximate layout of Logitech G920 Wheel:
#        _____       _____
#       |  ↑  \_____/  Y  |
#    L  | ← →  /   \  X B |  R
#    P  |  ↓  /     \  A  |  P
#       -----/       \-----
#      /  ⍄  |       |  ≡  \
#     /_LS___|       |___RS_\
#
#                G
#
# Apart from steering/accel/brake/clutch axis, no restrictions as ETS does not recognise this as suitable for UI control
# TODO: 'LS' and 'RS' were previously reserved for Range/Splitter, but this has now been moved to a stick with the dashboard controls duplicated.

#from evdev import ecodes
from evdev import InputDevice, categorize, ecodes, uinput
from hatlatch import HatLatch
from truckmode import TruckMode

# NB: Hat controls assume that L+R and U+D won't happen when issuing syn() calls - these may be delayed on a broken hat

import os, sys

DoQuit = False

def signalHandler(signum, frame):
    global DoQuit
    print('Recieved signal ' + str(signum))
    DoQuit = True

def main():
    signal.signal(signal.SIGTERM, signalHandler)
    signal.signal(signal.SIGINT, signalHandler)

    WheelInputDevice = evdev.InputDevice('/dev/input/by-id/usb-Logitech_G920_Driving_Force_Racing_Wheel_for_Xbox_One_0000ec9dbba366fa-event-joystick')
    VirtualUInput = evdev.uinput.UInput(name='PyTruckWheel Composite Input')

    truckmode = TruckMode(VirtualUInput)

    print('Listening for input events...')
    while not DoQuit:
        while (event := WheelInputDevice.read_one()) != None:
            truckmode.handleEvent(event)

        truckmode.tick()
        time.sleep(0.1)

    return 0

if __name__ == '__main__':
    sys.exit(main())
