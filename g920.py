from evdev import ecodes

BUTTON_RIGHTPADDLE=ecodes.BTN_TOP2 # 292
BUTTON_LEFTPADDLE=ecodes.BTN_PINKIE # 293

BUTTON_LSB=297 # ecodes.BTN_BASE4 # 297
BUTTON_RSB=296 # ecodes.BTN_BASE3 # 296

BUTTON_VIEW=295 # ecodes.BTN_BASE2 # 295 - has two overlapping squares
BUTTON_MENU=294 # ecodes.BTN_BASE - 3x lines
BUTTON_XBOX=298 # ecodes.BTN_BASE5

BUTTON_A=288 # ecodes.BTN_TRIGGER
BUTTON_B=289 # ecodes.BTN_THUMB
BUTTON_X=290 # ecodes.BTN_THUMB2
BUTTON_Y=291 # ecodes.BTN_TOP

HAT_X = ecodes.ABS_HAT0X
HAT_Y = ecodes.ABS_HAT0Y

PEDAL_DEADZONE=255 - 15

ACCELERATOR_AXIS = ecodes.ABS_Y
BRAKE_AXIS = ecodes.ABS_Z
CLUTCH_AXIS = ecodes.ABS_RZ

# d-pad notes:
# left/right is type 3 (EV_ABS), code 16 (ABS_HAT0X), value -1 (left) value 1 (right)
# up/down is type 3 (EV_ABS), code 17 (ABS_HAT0Y), value -1 (up) value 1 (down)

# pedal notes (although this script will likely leave them as-is):
# accelerator is type 3 (EV_ABS), code 1 (ABS_Y), value 0 at max, 255 at min
# brake is type 3 (EV_ABS), code 2 (ABS_Z), value 0 at max, 255 at min
# clutch is type 3 (EV_ABS), code 5 (ABS_RZ), value 0 at max (engaged), 255 at min (disengaged)

# PEDAL_DEADZONE=?
