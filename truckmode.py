import g920
import ets
from hatlatch import HatLatch
from evdev import InputDevice, categorize, ecodes, uinput
import time

class TruckMode:
    HAT_LEFT=-1
    HAT_RIGHT=1
    HAT_UP=-1
    HAT_DOWN=1
    HAT_NONE=0

    PRESS=1
    RELEASE=0



    def __init__(self, uinput):
        self.uinput = uinput
        self.HatLeftControl = HatLatch(uinput)
        self.HatRightControl = HatLatch(uinput)
        self.HatUpControl = HatLatch(uinput)
        self.HatDownControl = HatLatch(uinput)

        self.CruiseControlHeld = False
        self.HandbrakeHeld = False
        self.CruiseControlLastPressed = 0
        self.HandbrakeLastPressed = 0
        self.EnginePowerLastPressed = 0

        self.HoldThreshold = 0.30
        # longer confirm for trailer action
        self.EngineHoldThreshold = 0.50

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.releaseHats():
            self.uinput.syn()

    def releaseHats(self):
        syncNeeded = False
        syncNeeded |= self.HatLeftControl.Release()
        syncNeeded |= self.HatRightControl.Release()
        syncNeeded |= self.HatUpControl.Release()
        syncNeeded |= self.HatDownControl.Release()
        return syncNeeded

    def handleEvent(self, event):
        if event.type == ecodes.EV_ABS:
            self.handleHatEvent(event)
            return

        if event.type != ecodes.EV_KEY:
            return

        if event.value == self.PRESS:
            if event.code == g920.BUTTON_XBOX:
                self.EnginePowerLastPressed = event.timestamp()
                return

            if event.code == g920.BUTTON_X:
                self.CruiseControlHeld = True
                self.CruiseControlLastPressed = event.timestamp()
                # Reset d-pad if needed (only x-axis, retains look l/r controls)
                # TODO: Change left/right to dashboard display modes
                '''
                DoSyn = self.HatLeftControl.Release() or self.HatRightControl.Release()
                if DoSyn:
                    self.uinput.syn()
                '''
                if self.releaseHats():
                    self.uinput.syn()
                return

            if event.code == g920.BUTTON_MENU:
                self.HandbrakeHeld = True
                self.HandbrakeLastPressed = event.timestamp()
                # Reset d-pad if needed
                if self.releaseHats():
                    self.uinput.syn()
                return

            if event.code == g920.BUTTON_VIEW:
                if(self.HandbrakeHeld):
                    self.uinput.write(ecodes.EV_KEY, ets.RouteAdvisorModes, self.PRESS)
                else:
                    self.uinput.write(ecodes.EV_KEY, ets.NextCamera, self.PRESS)
                self.uinput.syn()
                return

        if event.value == self.RELEASE:
            if event.code == g920.BUTTON_XBOX:
                if event.timestamp() < (self.EnginePowerLastPressed + self.EngineHoldThreshold):
                    self.uinput.write(ecodes.EV_KEY, ets.Engine, self.PRESS)
                    self.uinput.write(ecodes.EV_KEY, ets.Engine, self.RELEASE)
                    self.uinput.syn()
                '''
                else:
                    self.uinput.write(ecodes.EV_KEY, ets.Trailer, self.PRESS)
                    self.uinput.write(ecodes.EV_KEY, ets.Trailer, self.RELEASE)
                self.uinput.syn()
                '''

                self.EnginePowerLastPressed = 0
                return

            if event.code == g920.BUTTON_X:
                self.CruiseControlHeld = False

                if event.timestamp() < (self.CruiseControlLastPressed + self.HoldThreshold):
                    self.uinput.write(ecodes.EV_KEY, ets.CruiseControl, self.PRESS)
                    self.uinput.write(ecodes.EV_KEY, ets.CruiseControl, self.RELEASE)
                    self.uinput.syn()

                self.CruiseControlLastPressed = 0
                return

            if event.code == g920.BUTTON_MENU:
                self.HandbrakeHeld = False

                if event.timestamp() < (self.HandbrakeLastPressed + self.HoldThreshold):
                    self.uinput.write(ecodes.EV_KEY, ets.Handbrake, self.PRESS)
                    self.uinput.write(ecodes.EV_KEY, ets.Handbrake, self.RELEASE)
                    self.uinput.syn()

                self.HandbrakeLastPressed = 0
                return

            if event.code == g920.BUTTON_VIEW:
                if self.HandbrakeHeld:
                    self.uinput.write(ecodes.EV_KEY, ets.RouteAdvisorModes, self.RELEASE)
                else:
                    self.uinput.write(ecodes.EV_KEY, ets.NextCamera, self.RELEASE)
                self.uinput.syn()
                return

    def handleHatEvent(self, event):
        if event.code == g920.HAT_X:
            if event.value == self.HAT_LEFT:
                self.HatRightControl.Release()
                if self.CruiseControlHeld:
                    self.HatLeftControl.Press(ets.Lights)
                    self.uinput.syn()
                elif self.HandbrakeHeld:
                    self.HatLeftControl.Press(ets.RouteAdvisorPreviousPage)
                    self.uinput.syn()
                else:
                    self.HatLeftControl.Press(ets.LookLeft)
                    self.uinput.syn()
            elif event.value == self.HAT_RIGHT:
                self.HatLeftControl.Release()
                if self.CruiseControlHeld:
                    self.HatRightControl.Press(ets.Wipers)
                    self.uinput.syn()
                elif self.HandbrakeHeld:
                    self.HatRightControl.Press(ets.RouteAdvisorNextPage)
                    self.uinput.syn()
                else:
                    self.HatRightControl.Press(ets.LookRight)
                    self.uinput.syn()
            else:
                if self.HatLeftControl.Release() or self.HatRightControl.Release():
                    self.uinput.syn()

        elif event.code == g920.HAT_Y:
            if event.value == self.HAT_UP:
                self.HatUpControl.Release()
                self.HatDownControl.Release()

                if self.HandbrakeHeld:
                    self.HatUpControl.Press(ets.RouteAdvisorNavigationZoomIn)
                elif self.CruiseControlHeld:
                    self.HatUpControl.Press(ets.CruiseControlIncrease)
                else:
                    self.HatUpControl.Press(ets.ChaseCamera)

                self.uinput.syn()
                self.uinput.syn()

            elif event.value == self.HAT_DOWN:
                self.HatUpControl.Release()
                if self.HandbrakeHeld:
                    self.HatDownControl.Press(ets.RouteAdvisorNavigationZoomOut)
                    self.uinput.syn()
                elif self.CruiseControlHeld:
                    self.HatDownControl.Press(ets.CruiseControlDecrease)
                    self.uinput.syn()
                else:
                    self.HatDownControl.Press(ets.InteriorCamera)
                    self.uinput.syn()
            else:
                self.HatUpControl.Release()
                self.HatDownControl.Release()
                self.uinput.syn()


    def tick(self):
        # TODO: Send trailer button press after 1 second of engine button

        if self.EnginePowerLastPressed != 0:
            if time.time() > (self.EnginePowerLastPressed + self.HoldThreshold):
                self.uinput.write(ecodes.EV_KEY, ets.Trailer, self.PRESS)
                self.uinput.write(ecodes.EV_KEY, ets.Trailer, self.RELEASE)
                self.uinput.syn()
                self.EnginePowerLastPressed = 0
        return
