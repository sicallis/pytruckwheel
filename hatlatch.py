from evdev import ecodes

PRESS=1
RELEASE=0

class HatLatch:
    def __init__(self, uinput):
        self.key = None
        self.state = RELEASE
        self.uinput = uinput

    def Press(self, key):
        if self.state == PRESS:
            raise Exception('Key is already pressed!')

        self.key = key
        self.uinput.write(ecodes.EV_KEY, self.key, PRESS)
        self.state = PRESS

    def Release(self):
        if self.state == RELEASE:
            return False

        self.uinput.write(ecodes.EV_KEY, self.key, RELEASE)
        self.state = RELEASE
        return True

    def __str__(self):
        return "{'key': " + str(self.key) + ", 'state': " + str(self.state) + "}"
