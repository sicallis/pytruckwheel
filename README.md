pytruckwheel
============

Python virtual composite controller for the Logitech G920 for use with American Truck Simulator or Euro Truck Simulator 2, using evdev.

This is an example of what can be done using evdev to add mode toggles, or additional press+hold functions to an existing gamepad/controller.

Controls
========

Approximate layout of Logitech G920 Wheel:
```
        _____       _____
       |  ↑  \_____/  Y  |
    L  | ← →  /   \  X B |  R
    P  |  ↓  /     \  A  |  P
       -----/       \-----
      /  ⍄  |       |  ≡  \
     /_LS___|       |___RS_\

                G
```

Current controls using sicallis' configuration:
* d-pad: left/right, look left/right
* d-pad: up: chase cam, down: interior cam
* menu: handbrake (if not held)
* view: next camera
* x: cruise control (if not held)
* G: engine on/off (if pressed), trailer attach/detach (if held)

Additional controls using pytruckwheel:
* if MENU held:
    * d-pad: left/right: previous/next route advisor page
    * d-pad: up/down: zoom in/out route advisor map page
    * view: route advisor mode
* if X held:
    * d-pad: left/right: dashboard info mode/map mode
    * d-pad: up/down: increase/decrease cruise control by step
    * view: next camera
    
This currently uses some additional buttons rebound to the keypad. A cleaner approach may be to bind directly to the pytruckwheel buttons.

Running
=======
To run:
    ./pytruckwheel.py
    
Adjust the `WheelInputDevice` variable in pytruckwheel.py to match your controller device.
